window.onload = () => {
  loadImgsWithJquery();
  // loadImgsWidthFetch();
  document.querySelector('#jqueryMoreBtn').addEventListener("click", moreListWithJquery);
  document.querySelector('#fetchMoreBtn').addEventListener("click", moreListWithFetch);
  // webpack 번들을 위해 addEventListener를 직접 선언 해줘야한다.
}
console.log('hello ajax');
// https://randomapi.com/api/dlagblvl?key=JG56-6WXO-5ZYN-K45S&results=25 //더미 데이터

let nowPage = 1;
const loadImgsWithJquery = () => {

  $.ajax({
    method: 'GET',
    url: "https://picsum.photos/v2/list?limit=3&page="+nowPage,
    contentType: 'application/json;charset=UTF-8',
    success: function(data){
      console.log('data갯수: '+data.length);
      let length = data.length;
      data.map(list => {
        let el = document.querySelector('.people-list');
        const temp = `
          <li class="bordered">
            <span class="tag jquery">jquery</span>
            <div class="img-wrap">
              <img src="`+list.download_url+`" alt="">
            </div>
            <ul class="">
              <li>
                <span class="label">아이디</span>
                <span class="id">`+list.id+`</span>
              </li>
              <li>
                <span class="label">이름</span>
                <span class="name">`+list.author+`</span>
              </li>
              <li>
                <span class="label">작품 사이즈</span>
                <span class="size">`+list.width + `x` +list.height+`</span>
              </li>
            </ul>
          </li>`
        el.insertAdjacentHTML('beforeend', temp);
      });
  
      nowPage += 1;
      console.log('jquery load 성공');
      
    },
    error: function(err){
      console.log(err);
    }
  });

}
const loadImgsWithFetch = () => {
  fetch("https://picsum.photos/v2/list?limit=3&page="+nowPage,{
    method: 'GET'
  })
  .then(res => res.json()).then((data) => {
    data.map(list => {
      let el = document.querySelector('.people-list');
      const temp = `
        <li class="bordered">
        <span class="tag fetch">Fetch</span>
          <div class="img-wrap">
            <img src="`+list.download_url+`" alt="">
          </div>
          <ul class="">
            <li>
              <span class="label">아이디</span>
              <span class="id">`+list.id+`</span>
            </li>
            <li>
              <span class="label">이름</span>
              <span class="name">`+list.author+`</span>
            </li>
            <li>
              <span class="label">작품 사이즈</span>
              <span class="size">`+list.width + `x` +list.height+`</span>
            </li>
          </ul>
        </li>`
      el.insertAdjacentHTML('beforeend', temp);
    });

    nowPage += 1;
    console.log('fetch load 성공');
    
  }).catch( err=>console.log(err) );
}



function moreListWithJquery(){
  console.log('more~!');
  loadImgsWithJquery();
}
function moreListWithFetch(){
  console.log('more~!');
  loadImgsWithFetch();
}
