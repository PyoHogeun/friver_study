const path = require('path');

module.exports = {
  entry:['@babel/polyfill','whatwg-fetch','./01.ajax-img-list/index.js'],
  output:{
    path: path.resolve(__dirname, 'dist/js'),
    filename: 'index.js'
  },
  module:{
    rules: [
      { 
        test: /\.js$/,
        exclude: "/node_modules/",
        use: "babel-loader", 
      }
    ],
  },
  mode: 'development'
}